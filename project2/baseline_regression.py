
import numpy as np
import pandas as pd
from sklearn.dummy import DummyRegressor

def baseline_regression(X, y, X_test,y_test):

    mean = y.mean()
    y_predicted = mean*np.ones(len(y_test))
    se = np.square(y_predicted-y_test)
    mse = np.sum(se)/len(y_test)

    return mse, y_predicted
