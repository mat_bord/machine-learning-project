import numpy as np
import pandas as pd
from matplotlib.pyplot import legend, semilogx, loglog, grid, title, subplot, hist, figure, plot, xlabel, ylabel, show
from matplotlib.pylab import imshow, colorbar, xticks, yticks, clim, title, subplot,figure,boxplot, plot, xlabel, ylabel, legend, show
from sklearn.metrics import confusion_matrix
from sklearn import model_selection
from sklearn.neighbors import KNeighborsClassifier, DistanceMetric
from sklearn.model_selection import train_test_split


#read the document
filename='dataset_train.xlsx'
df=pd.read_excel(filename)
df_use = df.copy()
df_use = df_use.drop(['blue','dual_sim','four_g','three_g','touch_screen','wifi'],axis=1)

#instead of get_values()
raw_data=df_use.to_numpy()
X=raw_data

#Extract attribute names by removing price range attributes + binary attribute
attributeNames = list (df_use.columns.values)

#feature transformation
#Get the area of the smartphone screen by screen height*screen width
width_idx = 12
height_idx = 11
area_screen = (X[:,width_idx]*X[:,height_idx]).reshape(-1,1)
area_screen_name='Screen_Area'

#Get the area of the smartphone in pixel
pxh_idx=8
pxw_idx=9
area_pixel = (X[:,pxh_idx]*X[:,pxw_idx]).reshape(-1,1)
area_pixel_name='Screen_Area(in Pixels)'

X=np.asarray(np.bmat('X,area_screen,area_pixel'))

#Insert `new_column` into `an_array` at index `1`
attributeNames = np.insert(attributeNames, 15, area_screen_name, axis=0)
attributeNames = np.insert(attributeNames, 16, area_pixel_name, axis=0)
attributeNames1 = list (attributeNames[0:14])
attributeNames2 = list (attributeNames[15:17])
attributeNames = attributeNames1 + attributeNames2

classLabels = raw_data[:,14]
classNames = np.unique(classLabels)
classNames=list(classNames)
classDict = dict(zip(classNames,range(len(classNames))))
y = np.array([classDict[cl] for cl in classLabels])

price_idx=14
X_cols = list(range(0,price_idx)) + list(range(price_idx,len(attributeNames)))
X=X[:,X_cols]

N,M=X.shape;
C = len(classNames)




# # K-nearest neighbors
# K=1
# for i in range(10):
    
#     #############K NEAREST NEIGHBOUR#############
#     #Plot the training data points (color-coded) and test data points.
#     figure()
#     styles = ['.b', '.r', '.g', '.y']
#     for c in range(C):
#         class_mask = (y_train==c)
#         plot(X_train[class_mask,0], X_train[class_mask,1], styles[c])
  
    
#     # Distance metric (corresponds to 2nd norm, euclidean distance).
#     # You can set dist=1 to obtain manhattan distance (cityblock distance).
#     dist=2
#     metric = 'minkowski'
#     metric_params = {} # no parameters needed for minkowski
    
#     # Fit classifier and classify the test points
#     knclassifier = KNeighborsClassifier(n_neighbors=K, p=dist, 
#                                         metric=metric,
#                                         metric_params=metric_params)
#     knclassifier.fit(X_train, y_train)
#     y_est = knclassifier.predict(X_test)
    
    
#     # Plot the classfication results    
#     styles = ['ob', 'or', 'og', 'oy']
#     for c in range(C):
#         class_mask = (y_est==c)
#         plot(X_test[class_mask,0], X_test[class_mask,1], styles[c], markersize=10)
#         plot(X_test[class_mask,0], X_test[class_mask,1], 'kx', markersize=8)
#     title('Synthetic data classification - KNN');
#     show()
#     # Compute and plot confusion matrix
#     cm = confusion_matrix(y_test, y_est);
#     correct = cm.diagonal().sum()
#     uncorrect = y_test.shape[0] - correct
#     errors_KNN[i][0]=K
#     errors_KNN[i][1]=uncorrect/y_test.shape[0]
#     accuracy = 100*cm.diagonal().sum()/cm.sum(); error_rate = 100-accuracy;
#     figure();
#     imshow(cm, cmap='binary', interpolation='None');
#     colorbar()
#     xticks(range(C)); yticks(range(C));
#     xlabel('Predicted class'); ylabel('Actual class');
#     title('Confusion matrix (Accuracy: {0}%, Error Rate: {1}%)'.format(accuracy, error_rate));
    
#     #############BASELINE#############
    
    
    
#     K+=1
    
# show()

# yhatA=y_est
# y_true=y_test

# print('K-NEAREST NEIGHBOR')
# for i in range(10):
#     print('Outer fold:',i+1,'k:',errors_KNN[i][0],'Error_test:',errors_KNN[i][1])
# Outer fold: 1 k: 1.0 Error_test: 0.1
# Outer fold: 2 k: 2.0 Error_test: 0.102
# Outer fold: 3 k: 3.0 Error_test: 0.084
# Outer fold: 4 k: 4.0 Error_test: 0.09
# Outer fold: 5 k: 5.0 Error_test: 0.076
# Outer fold: 6 k: 6.0 Error_test: 0.074
# Outer fold: 7 k: 7.0 Error_test: 0.07
# Outer fold: 8 k: 8.0 Error_test: 0.072
# Outer fold: 9 k: 9.0 Error_test: 0.07
# Outer fold: 10 k: 10.0 Error_test: 0.068