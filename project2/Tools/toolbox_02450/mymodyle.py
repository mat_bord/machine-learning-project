# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 14:52:36 2020

@author: laura
"""

def myfunction(n=int):
    """myfunction will return an array of values ranging
    from 1 to the input integer n."""
    x = range(1,n+1)
    return list(x)