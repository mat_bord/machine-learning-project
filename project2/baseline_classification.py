

import array

#finding most frequest class of price range
def most_frequent(List): 
    counter = 0
    num = List[0] 
      
    for i in List: 
        curr_frequency = List.count(i) 
        if(curr_frequency> counter): 
            counter = curr_frequency 
            num = i 
  
    return num 

def baseline_classification(X,y,X_test,y_test) :
    y=y.tolist()
    most_common=most_frequent(y)
    error_rate=0
    #predicting y for the largest class
    y_predicted = array.array('i',[most_common] * len(y_test))
   
    for i in range(len(y_test)):
        if(y_test[i] != y_predicted[i]):
            error_rate+=1
    
    return error_rate, y_predicted

    



