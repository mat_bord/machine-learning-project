from regression_partb import *
import numpy as np, scipy.stats as st
import scipy.stats

test_proportion=0.2
alpha = 0.05

##Model A : Linear Regression
#compute confidence interval of model A
CIA = st.t.interval(1-alpha, df=len(zA)-1, loc=np.mean(zA), scale=st.sem(zA))  # Confidence interval
#(0.5240316913218179, 0.7331948559763377)

##Model B : Artificial Neural Networks
#compute confidence interval of model B
CIB = st.t.interval(1-alpha, df=len(zB)-1, loc=np.mean(zB), scale=st.sem(zB))  # Confidence interval
#(0.83054062, 1.01675252)

##Model C : Baseline
#compute confidence interval of model C
CIC = st.t.interval(1-alpha, df=len(zC)-1, loc=np.mean(zC), scale=st.sem(zC))  # Confidence interval
#(0.8829516114050155, 1.133696976986328)

##Compute confidence interval of z1 = zA - zB and p-value of null hypothesis
#ANN vs Linear regression
z1 = zA - zB
CI1 = st.t.interval(1-alpha, len(z1)-1, loc=np.mean(z1), scale=st.sem(z1))  # Confidence interval
p1 = st.t.cdf( -np.abs( np.mean(z1) )/st.sem(z1), df=len(z1)-1)  # p-value
#(-0.38813925, -0.20192734)
#p-value = 4.34075329e-10

##Compute confidence interval of z2 = zB - zC and p-value of null hypothesis
#ANN vs Baseline
z2 = zB - zC
CI2 = st.t.interval(1-alpha, len(z2)-1, loc=np.mean(z2), scale=st.sem(z2))  # Confidence interval
p2 = st.t.cdf( -np.abs( np.mean(z2) )/st.sem(z2), df=len(z2)-1)  # p-value
#(-0.17778368, 0.00842823)
#p-value = 0.03729423

##Compute confidence interval of z3 = zA - zC and p-value of null hypothesis
#Linear regression vs Baseline
z3 = zA - zC
CI3 = st.t.interval(1-alpha, len(z3)-1, loc=np.mean(z3), scale=st.sem(z3))  # Confidence interval
p3 = st.t.cdf( -np.abs( np.mean(z3) )/st.sem(z3), df=len(z3)-1)  # p-value
#(-0.49285819288545685, -0.2665638482077314)
#p-value = 1.6529136346730522e-10