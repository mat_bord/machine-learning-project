from classification_comparaison import * 
from toolbox_02450 import mcnemar

##Model A : K-NEAREST NEIGHBOR
##Model B : BASELINE
##Model C : MULTINOMIAL REGRESSION

alpha = 0.05

##MODEL A VS B
#Compute the Jeffreys interval
[thetahat1, CI1, p1] = mcnemar(y_true, yhatA.squeeze(), yhatB.squeeze(), alpha=alpha)
print("theta = theta_A-theta_B point estimate", thetahat1, " CI: ", CI1, "p-value", p1)
#theta = 0.7065
#CI = (0.6845633101739723, 0.7277866240001636)
#p-value = 0.0

##MODEL A VS C
#Compute the Jeffreys interval
[thetahat2, CI2, p2] = mcnemar(y_true, yhatA.squeeze(), yhatC.squeeze(), alpha=alpha)
print("theta = theta_A-theta_C point estimate", thetahat2, " CI: ", CI2, "p-value", p2)
#theta = -0.047
#CI = (-0.058571889116652676, -0.03542188635034571)
#p-value = 7.248285966038657e-16

##MODEL B VS C
#Compute the Jeffreys interval
[thetahat3, CI3, p3] = mcnemar(y_true, yhatB.squeeze(), yhatC.squeeze(), alpha=alpha)
print("theta = theta_B-theta_C point estimate", thetahat3, " CI: ", CI3, "p-value", p3)
#theta = -0.7535 
#CI = (-0.7725588018776677, -0.7337950456119997)
# p-value = 0.0
