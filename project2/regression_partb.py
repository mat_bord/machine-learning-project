from regression import *
from sklearn import model_selection
import scipy.stats as stats
from toolbox_02450 import rlr_validate
import numpy as np
import sklearn.linear_model as lm
from baseline_regression import *
from neural_network import neural_network

attributeNames = [u'Offset']+attributeNames
M = M+1
## Crossvalidation
# Create crossvalidation partition for evaluation
K = 10
CV = model_selection.KFold(K, shuffle=True)
#CV = model_selection.KFold(K, shuffle=False)

# Values of lambda
lambdas = np.power(10.,range(-2,7))

# Initialize variables
#T = len(lambdas)
Error_train = np.empty((K,1))
Error_test = np.empty((K,1))
Error_train_rlr = np.empty((K,1))
Error_test_rlr = np.empty((K,1))
Error_train_nofeatures = np.empty((K,1))
Error_test_nofeatures = np.empty((K,1))
w_rlr = np.empty((M,K))
mu = np.empty((K, M-1))
sigma = np.empty((K, M-1))
w_noreg = np.empty((M,K))

errors_ANN=np.zeros((10,2))
errors_linear_regression=np.zeros((10,2))
errors_baseline=np.zeros(10)

k=0

for train_index, test_index in CV.split(X,y):
    #######ARTIFICIAL NEURAL NETWORKS#######
    
    mB, yhatB, y_test, test_error, h_opt = neural_network(X,y)
    y_test.detach().numpy()    
    errors_ANN[k][0]=h_opt
    errors_ANN[k][1]=test_error
    print("Outer fold:",k,"Hidden Unit Index:",h_opt,"Error_test:",test_error)
    
    ##t-test_part1
    # compute z with squared error
    zB = np.abs(y_test - yhatB) ** 2
    zB = zB.detach().numpy()    

    #######LINEAR REGRESSION#######
    
    #Add offsett attribute
    X_lr = np.concatenate((np.ones((X.shape[0],1)),X),1)
    
    
    # extract training and test set for current CV fold
    X_train = X_lr[train_index]
    y_train = y[train_index]
    X_test = X_lr[test_index]
    y_test = y[test_index]
    internal_cross_validation = 10   
    
    opt_val_err, opt_lambda, mean_w_vs_lambda, train_err_vs_lambda, test_err_vs_lambda = rlr_validate(X_train, y_train, lambdas, internal_cross_validation)
    
    # Standardize outer fold based on training set, and save the mean and standard
    # deviations since they're part of the model (they would be needed for
    # making new predictions) - for brevity we won't always store these in the scripts
    mu[k, :] = np.mean(X_train[:, 1:], 0)
    sigma[k, :] = np.std(X_train[:, 1:], 0)
    
    X_train[:, 1:] = (X_train[:, 1:] - mu[k, :] ) / sigma[k, :] 
    X_test[:, 1:] = (X_test[:, 1:] - mu[k, :] ) / sigma[k, :] 
    
    Xty = X_train.T @ y_train
    XtX = X_train.T @ X_train
    
    # Compute mean squared error without using the input data at all
    Error_train_nofeatures[k] = np.square(y_train-y_train.mean()).sum(axis=0)/y_train.shape[0]
    Error_test_nofeatures[k] = np.square(y_test-y_test.mean()).sum(axis=0)/y_test.shape[0]

    # Estimate weights for the optimal value of lambda, on entire training set
    lambdaI = opt_lambda * np.eye(M)
    lambdaI[0,0] = 0 # Do no regularize the bias term
    w_rlr[:,k] = np.linalg.solve(XtX+lambdaI,Xty).squeeze()
    # Compute mean squared error with regularization with optimal lambda
    Error_train_rlr[k] = np.square(y_train-X_train @ w_rlr[:,k]).sum(axis=0)/y_train.shape[0]
    Error_test_rlr[k] = np.square(y_test-X_test @ w_rlr[:,k]).sum(axis=0)/y_test.shape[0]
    print('Outer fold:',k+1,'Optimal lambda:',opt_lambda,'Error_test:',Error_test_rlr[k])
    errors_linear_regression[k][0]=opt_lambda
    errors_linear_regression[k][1]=Error_test_rlr[k]

    ###t-test_part1
    mA = lm.LinearRegression().fit(X_train, y_train)
    yhatA = mA.predict(X_test)
    #compute z with squared error
    zA = np.abs(y_test - yhatA) ** 2
    
    Error_train[k] = np.square(y_train-mA.predict(X_train)).sum()/y_train.shape[0]
    Error_test[k] = np.square(y_test-mA.predict(X_test)).sum()/y_test.shape[0]

    # Display the results for the last cross-validation fold
    if k == K-1:
        figure(k, figsize=(12,8))
        subplot(1,2,1)
        semilogx(lambdas,mean_w_vs_lambda.T[:,1:],'.-') # Don't plot the bias term
        xlabel('Regularization factor')
        ylabel('Mean Coefficient Values')
        grid()
        
        subplot(1,2,2)
        title('Optimal lambda: 1e{0}'.format(np.log10(opt_lambda)))
        loglog(lambdas,train_err_vs_lambda.T,'b.-',lambdas,test_err_vs_lambda.T,'r.-')
        xlabel('Regularization factor')
        ylabel('Squared error (crossvalidation)')
        legend(['Train error','Validation error'])
        grid()
        
    
   
    
    #######BASELINE#######
    
    X_train = X[train_index] 
    y_train = y[train_index]
    X_test = X[test_index]
    y_test = y[test_index]
    
    errors_baseline[k], y_predicted=baseline_regression(X_train, y_train, X_test, y_test)
  
    ##t-test_part1
    yhatC = y_predicted
    #compute z with squared error
    zC = np.abs(y_test - yhatC) ** 2

    k+=1


##DISPLAY RESULTS##
show()

####LINEAR REGRESSION TABLE####
print("LINEAR REGRESSION TABLE")
for k in range(10):
    print("Outer fold:",k+1,"Optimal lambda:",errors_linear_regression[k,0],"Error_test:",errors_linear_regression[k,1])
# Outer fold: 1 Optimal lambda: 0.1 Error_test: [0.67293994]
# Outer fold: 2 Optimal lambda: 0.1 Error_test: [0.59667588]
# Outer fold: 3 Optimal lambda: 0.01 Error_test: [0.72687928]
# Outer fold: 4 Optimal lambda: 1.0 Error_test: [0.64659323]
# Outer fold: 5 Optimal lambda: 0.1 Error_test: [0.61024204]
# Outer fold: 6 Optimal lambda: 0.1 Error_test: [0.60883027]
# Outer fold: 7 Optimal lambda: 1.0 Error_test: [0.80785697]
# Outer fold: 8 Optimal lambda: 0.1 Error_test: [0.80980478]
# Outer fold: 9 Optimal lambda: 0.01 Error_test: [0.61988115]
# Outer fold: 10 Optimal lambda: 1.0 Error_test: [0.68845229]

####ARTIFICIAL NEURAL NETWORKS####
print("ARTIFICIAL NEURAL NETWORKS")
for k in range(10):
    print("Outer fold:",k+1,"Hidden Unit Index:",errors_ANN[k,0],"Error_test:",errors_ANN[k,1])
# ARTIFICIAL NEURAL NETWORKS
# Outer fold: 1 Hidden Unit Index: 2.0 Error_test: 0.7020999789237976
# Outer fold: 2 Hidden Unit Index: 2.0 Error_test: 0.6804999709129333
# Outer fold: 3 Hidden Unit Index: 1.0 Error_test: 0.6848999857902527
# Outer fold: 4 Hidden Unit Index: 1.0 Error_test: 0.6863999962806702
# Outer fold: 5 Hidden Unit Index: 1.0 Error_test: 0.6880999803543091
# Outer fold: 6 Hidden Unit Index: 2.0 Error_test: 0.7013000249862671
# Outer fold: 7 Hidden Unit Index: 1.0 Error_test: 0.6897000074386597
# Outer fold: 8 Hidden Unit Index: 1.0 Error_test: 0.6804999709129333
# Outer fold: 9 Hidden Unit Index: 2.0 Error_test: 0.6747999787330627
# Outer fold: 10 Hidden Unit Index: 1.0 Error_test: 0.6812999844551086

####BASELINE
print("BASELINE")
for k in range(10):
    print("Outer fold:",k+1,"Error:",round(errors_baseline[k],4))
# BASELINE
# Outer fold: 1 Error: 0.964
# Outer fold: 2 Error: 0.9929
# Outer fold: 3 Error: 0.9935
# Outer fold: 4 Error: 0.9809
# Outer fold: 5 Error: 1.1538
# Outer fold: 6 Error: 0.9854
# Outer fold: 7 Error: 0.9709
# Outer fold: 8 Error: 1.0799
# Outer fold: 9 Error: 0.9284
# Outer fold: 10 Error: 0.9634
