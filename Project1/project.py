# -*- coding: utf-8 -*-
"""
Created on Sun Sep 13 14:33:51 2020

@author: laura
"""

import numpy as np
import xlrd
from scipy.linalg import svd
import matplotlib.pyplot as plt
import pandas as pd

doc = xlrd.open_workbook('dataset_train.xlsx').sheet_by_index(0)
filename='train.csv'
df=pd.read_csv(filename) 

# Extract attribute names
attributeNames = doc.row_values(rowx=0, start_colx=0, end_colx=21)

# Preallocate memory, then extract data to matrix X
X = np.empty((2000,21))
for i in range(21):
    X[:,i] = np.array(doc.col_values(i,1,2001)).T
 
# Extract vector y (price range), convert to NumPy array
y = np.asarray(X[:,-1])
# Extract N rowSize, M colSize
N,M=X.shape;

# Boxplots
plt.figure(figsize=(20, 10), num='Boxplots')
plt.figure;
plt.subplot(331)
plt.boxplot(X[:,0])
plt.ylabel('Number of  mAh')
plt.title('Boxplot of Power battery')

plt.subplot(332)
plt.boxplot(X[:,2])
plt.ylabel('speed ')
plt.title('Boxplot of clock speed')

plt.subplot(333)
plt.boxplot(X[:,4])
plt.ylabel('Number of megapixels')
plt.title('Boxplot of Front camera')

plt.subplot(334)
plt.boxplot(X[:,6])
plt.ylabel('Number of Gigabytes')
plt.title('Boxplot of Internal memory')

plt.subplot(335)
plt.boxplot(X[:,7])
plt.ylabel('Number of centimetres')
plt.title('Boxplot of Mobile depth')

plt.subplot(336)
plt.boxplot(X[:,8])
plt.ylabel('Number of grammes')
plt.title('Boxplot of Mobile weight')

plt.subplot(337)
plt.boxplot(X[:,9])
plt.ylabel('Number of cores of processor')
plt.title('Boxplot of Processor')

plt.subplot(338)
plt.boxplot(X[:,10])
plt.ylabel('Number of mepapixels')
plt.title('Boxplot of primary camera')

plt.subplot(339)
plt.boxplot(X[:,11])
plt.ylabel('Number of pixel resolution')
plt.title('Boxplot of Pixel height')
plt.show()

plt.figure(figsize=(20, 10), num='Boxplots')
plt.subplot(231)
plt.boxplot(X[:,12])
plt.ylabel('Number of pixel resolution')
plt.title('Boxplot of Pixel width')

plt.subplot(232)
plt.boxplot(X[:,13])
plt.ylabel('Number of megabytes')
plt.title('Boxplot of RAM')

plt.subplot(233)
plt.boxplot(X[:,14])
plt.ylabel('Number of centimetres')
plt.title('Boxplot of screen height')

plt.subplot(234)
plt.boxplot(X[:,15])
plt.ylabel('Number of centimetres')
plt.title('Boxplot of screen width')

plt.subplot(235)
plt.boxplot(X[:,16])
plt.ylabel('Number of hours')
plt.title('Boxplot of Talk time')
plt.show()


# Calculate and display covariance matrix
cov = np.cov(X, rowvar=False)
cov = cov.round(2)

fig, ax = plt.subplots()
im = ax.imshow(cov, aspect='equal', cmap='RdYlBu_r', vmin=-25, vmax=25, origin='upper')
ax.set_xticks(np.arange(M))
ax.set_yticks(np.arange(M))
ax.set_xticklabels(attributeNames)
ax.set_yticklabels(attributeNames)
values = np.unique(cov.ravel())

plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
fig.colorbar(im);
plt.show()

ax.set_title("Covariance of the different variables")
fig.canvas.set_window_title('Covariance')
plt.show()

# Calculate and display correlation matrix
corr = np.corrcoef(X, rowvar=False)
corr = corr.round(2)

fig, ax = plt.subplots()
im = ax.imshow(corr, aspect= 'equal', cmap='Oranges', origin='upper')
ax.set_xticks(np.arange(M))
ax.set_yticks(np.arange(M))
ax.set_xticklabels(attributeNames)
ax.set_yticklabels(attributeNames)

plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
fig.colorbar(im);
ax.set_title("Correlation of the different variables")
fig.canvas.set_window_title('Correlation')
plt.show()

# Subtract the mean from the data and divide by the attribute standard
# deviation to obtain a standardized dataset:
Y = X - np.ones((N, 1))*X.mean(0)
Y = Y*(1/np.std(Y,0))

# PCA by computing SVD of Y
U,S,Vh = svd(Y,full_matrices=False)
V=Vh.T # For the direction of V to fit the convention in the course we transpose
Z = Y @ V;
# Compute variance explained by principal components
rho = (S*S) / (S*S).sum() 
threshold = 0.9
   
# Plot cumulative variance explained
plt.plot(range(1,len(rho)+1),rho,'x-')
plt.plot(range(1,len(rho)+1),np.cumsum(rho),'o-')
plt.plot([1,len(rho)],[threshold, threshold],'k--')
plt.title('Variance explained by principal components');
plt.xlabel('Principal component');
plt.ylabel('Variance explained');
plt.legend(['Individual','Cumulative','Threshold'])
plt.grid()
plt.title('PCA Variance explained')
plt.show()


#Select the PC1 and PC2
i=0;
j=1;
################################# PLOT attribute coefficients

for att in range(V.shape[1]):    
    plt.arrow(0,0, V[att,i], V[att,j])
    plt.text(V[att,i], V[att,j], attributeNames[att])
    plt.xlim([-1,1])
    plt.ylim([-1,1])
    plt.xlabel('PC'+str(i+1))
    plt.ylabel('PC'+str(j+1))
    plt.grid()
    # Add a unit circle
    plt.plot(np.cos(np.arange(0, 2*np.pi, 0.01)), 
         np.sin(np.arange(0, 2*np.pi, 0.01)));
    plt.title('Attribute coefficients')
    plt.axis('equal')
    plt.show()

################################ Plot projection

C = len(attributeNames)
for c in range(C):
    plt.plot(Z[y==c,i], Z[y==c,j], 'o', alpha=.5)
plt.xlabel('PC'+str(i+1))
plt.ylabel('PC'+str(j+1))
plt.title('Smartphone : PCA' )
plt.legend(['low cost','medium cost','high cost','very high cost'])
plt.axis('equal')

plt.show()

#Analysis

for value in range(C):
    X_value = X[:,value]
    
    # Compute values
    mean_x = X_value.mean()
    std_x = X_value.std(ddof=1)
    median_x = np.median(X_value)
    range_x = X_value.max()-X_value.min()

    # Display results
    print('Attribute : ', attributeNames[value])
    print('Vector:',X_value)
    print('Mean:',mean_x)
    print('Standard Deviation:',std_x)
    print('Median:',median_x)
    print('Range:',range_x)
    print('\n')


