
import numpy as np
import pandas as pd
from matplotlib.pyplot import legend, semilogx, loglog, grid, title, subplot, hist, figure, plot, xlabel, ylabel, show
import sklearn.linear_model as lm
from sklearn import preprocessing

#read the document
filename='dataset_train.xlsx'
df=pd.read_excel(filename)

#instead of get_values()
raw_data=df.to_numpy()
X=raw_data

#apply feature transformation to X such that each column has mean
#0 and standard deviation 1
X=preprocessing.scale(X)

#get Attribute names
attributeNames=np.asarray(df.columns.values)

#feature transformation
#Get the area of the smartphone screen by screen height*screen width
width_idx = 15
height_idx = 14
area_screen = (X[:,width_idx]*X[:,height_idx]).reshape(-1,1)
area_screen_name='Screen_Area'

#Get the area of the smartphone in pixel
pxh_idx=11
pxw_idx=12
area_pixel = (X[:,pxh_idx]*X[:,pxw_idx]).reshape(-1,1)
area_pixel_name='Screen_Area(in Pixels)'

X=np.asarray(np.bmat('X,area_screen,area_pixel'))

#Insert `new_column` into `an_array` at index `1`
attributeNames = np.insert(attributeNames, 21, area_screen_name, axis=0)
attributeNames = np.insert(attributeNames, 22, area_pixel_name, axis=0)

attributeNames = list (attributeNames[1:23])

# #split dataset into features and target vector
#battery_idx = np.where(attributeNames=='battery_power')
#battery_idx = battery_idx[0][0]
battery_idx=0
y=X[:,battery_idx]

X_cols = list(range(0,battery_idx)) + list(range(battery_idx+1,len(attributeNames)+1))
X=X[:,X_cols]

N,M = X.shape

#Show histogram
# figure()
# for i in range(23) :
#     figure()
#     hist(X[:,i])
#     xlabel(attributeNames[i])
   