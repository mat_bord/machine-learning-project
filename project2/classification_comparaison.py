
from classification import *
from sklearn import model_selection
import scipy.stats as stats
from toolbox_02450 import rlr_validate
import numpy as np
import sklearn.linear_model as lm
from baseline_classification import baseline_classification


K = 10
CV = model_selection.KFold(K, shuffle=True)

# Values of lambda
lambdas = np.power(10.,range(-2,7))

# Initialize variables
#T = len(lambdas)
Error_train = np.empty((K,1))
Error_test = np.empty((K,1))
Error_train_rlr = np.empty((K,1))
Error_test_rlr = np.empty((K,1))
Error_train_nofeatures = np.empty((K,1))
Error_test_nofeatures = np.empty((K,1))
test_error_rate=np.empty((K,1))
w_rlr = np.empty((M,K))
mu = np.empty((K, M-1))
sigma = np.empty((K, M-1))
w_noreg = np.empty((M,K))

errors_KNN=np.zeros((10,2))
errors_multinomial=np.zeros((10,2))
errors_baseline=np.zeros(10)

k=0
# store predictions.
yhatA = []
yhatB = []
yhatC = []
y_true = []


for train_index, test_index in CV.split(X,y):
    
    #############K NEAREST NEIGHBOUR#############
    X_train = X[train_index]
    y_train = y[train_index]
    X_test = X[test_index]
    y_test = y[test_index]
    
    
    # Distance metric (corresponds to 2nd norm, euclidean distance).
    # You can set dist=1 to obtain manhattan distance (cityblock distance).
    dist=2
    metric = 'minkowski'
    metric_params = {} # no parameters needed for minkowski
    # Fit classifier and classify the test points (consider 1 to 40 neighbors)
    dyA = []
    
    # Fit classifier and classify the test points
    knclassifier = KNeighborsClassifier(n_neighbors=k+1, p=dist, 
                                        metric=metric,
                                        metric_params=metric_params)
    knclassifier.fit(X_train, y_train)
    y_est = knclassifier.predict(X_test)
    dyA.append( y_est )
    dyA = np.stack(dyA, axis=1)
    yhatA.append(dyA)
        
    # Compute and plot confusion matrix
    cm = confusion_matrix(y_test, y_est);
    correct = cm.diagonal().sum()
    uncorrect = y_test.shape[0] - correct
    
    errors_KNN[k][0]=k+1
    errors_KNN[k][1]=uncorrect/y_test.shape[0]
    accuracy = 100*cm.diagonal().sum()/cm.sum(); error_rate = 100-accuracy;
    
    #############BASELINE#############
    dyB = []
    errors_baseline[k], y_predicted = baseline_classification(X_train, y_train, X_test, y_test)
    errors_baseline[k] = errors_baseline[k]/y_test.shape[0]
    
    dyB.append(y_predicted)
    dyB = np.stack(dyB, axis=1)
    yhatB.append(dyB)
    
    #############MULTINOMIAL REGRESSION#############
    dyC = []
    internal_cross_validation = 10    
    
    opt_val_err, opt_lambda, mean_w_vs_lambda, train_err_vs_lambda, test_err_vs_lambda = rlr_validate(X_train, y_train, lambdas, internal_cross_validation)
    
    # Standardize outer fold based on training set, and save the mean and standard
    # deviations since they're part of the model (they would be needed for
    # making new predictions) - for brevity we won't always store these in the scripts
    mu[k, :] = np.mean(X_train[:, 1:], 0)
    sigma[k, :] = np.std(X_train[:, 1:], 0)
    
    X_train[:, 1:] = (X_train[:, 1:] - mu[k, :] ) / sigma[k, :] 
    X_test[:, 1:] = (X_test[:, 1:] - mu[k, :] ) / sigma[k, :] 
    
    Xty = X_train.T @ y_train
    XtX = X_train.T @ X_train

    # Estimate weights for the optimal value of lambda, on entire training set
    lambdaI = opt_lambda * np.eye(M)
    lambdaI[0,0] = 0 # Do no regularize the bias term
    w_rlr[:,k] = np.linalg.solve(XtX+lambdaI,Xty).squeeze()
    # Compute mean squared error with regularization with optimal lambda
    Error_train_rlr[k] = np.square(y_train-X_train @ w_rlr[:,k]).sum(axis=0)/y_train.shape[0]
    Error_test_rlr[k] = np.square(y_test-X_test @ w_rlr[:,k]).sum(axis=0)/y_test.shape[0]


    m = lm.LogisticRegression(multi_class='multinomial', solver='newton-cg', max_iter=100).fit(X_train, y_train)
    
    Error_train[k] = np.square(y_train-m.predict(X_train)).sum()/y_train.shape[0]
    Error_test[k] = np.square(y_test-m.predict(X_test)).sum()/y_test.shape[0]
    
    y_test_est = m.predict(X_test)
    
    dyC.append( y_test_est )
    dyC = np.stack(dyC, axis=1)
    yhatC.append(dyC)
    
    test_error_rate = np.sum(y_test_est!=y_test) / len(y_test)
    errors_multinomial[k][0]=opt_lambda
    errors_multinomial[k][1]=test_error_rate
    
    y_true.append(y_test)
    
    k+=1

yhatA = np.concatenate(yhatA)
yhatB = np.concatenate(yhatB)
yhatC = np.concatenate(yhatC)
y_true = np.concatenate(y_true)

    
####MULTINOMIAL REGRESSION####
print("MULTINOMIAL REGRESSION TABLE")
for k in range(10):
    print("Outer fold:",k+1,"Optimal lambda:",errors_multinomial[k,0],"Error_test:",errors_multinomial[k,1])

print('Weights in last fold:')
for m in range(M):
    print('{:>16} {:>16}'.format(attributeNames[m], np.round(w_rlr[m,-1],2)))
  #   Weights in last fold:
  # battery_power             0.0
  #   clock_speed           -0.01
  #            fc            -0.0
  #    int_memory            0.01
  #         m_dep           -0.01
  #     mobile_wt           -0.03
  #       n_cores            0.02
  #            pc            -0.0
  #     px_height            0.11
  #      px_width            0.13
  #           ram            1.03
  #          sc_h            0.01
  #          sc_w             0.0
  #   Screen_Area           -0.01
  
# Outer fold: 1 Optimal lambda: 1.0 Error_test: 0.045
# Outer fold: 2 Optimal lambda: 1.0 Error_test: 0.0
# Outer fold: 3 Optimal lambda: 1.0 Error_test: 0.025
# Outer fold: 4 Optimal lambda: 1.0 Error_test: 0.025
# Outer fold: 5 Optimal lambda: 1.0 Error_test: 0.05
# Outer fold: 6 Optimal lambda: 1.0 Error_test: 0.025
# Outer fold: 7 Optimal lambda: 1.0 Error_test: 0.055
# Outer fold: 8 Optimal lambda: 1.0 Error_test: 0.045
# Outer fold: 9 Optimal lambda: 0.01 Error_test: 0.045
# Outer fold: 10 Optimal lambda: 1.0 Error_test: 0.04

####K NEAREST NEIGHBOUR####
print("K NEAREST NEIGHBOUR")
for k in range(10):
    print("Outer fold:",k+1,"K:",errors_KNN[k,0],"Error_test:",errors_KNN[k,1])
# Outer fold: 1 K: 1.0 Error_test: 0.08
# Outer fold: 2 K: 2.0 Error_test: 0.1
# Outer fold: 3 K: 3.0 Error_test: 0.075
# Outer fold: 4 K: 4.0 Error_test: 0.105
# Outer fold: 5 K: 5.0 Error_test: 0.075
# Outer fold: 6 K: 6.0 Error_test: 0.065
# Outer fold: 7 K: 7.0 Error_test: 0.085
# Outer fold: 8 K: 8.0 Error_test: 0.075
# Outer fold: 9 K: 9.0 Error_test: 0.045
# Outer fold: 10 K: 10.0 Error_test: 0.07

####BASELINE
print("BASELINE")
for k in range(10):
    print("Outer fold:",k+1,"Error:",round(errors_baseline[k],4))
# BASELINE
# Outer fold: 1 Error: 0.795
# Outer fold: 2 Error: 0.775
# Outer fold: 3 Error: 0.77
# Outer fold: 4 Error: 0.78
# Outer fold: 5 Error: 0.78
# Outer fold: 6 Error: 0.765
# Outer fold: 7 Error: 0.79
# Outer fold: 8 Error: 0.79
# Outer fold: 9 Error: 0.785
# Outer fold: 10 Error: 0.775

