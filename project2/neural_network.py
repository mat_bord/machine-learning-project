

import matplotlib.pyplot as plt
import numpy as np
from scipy.io import loadmat
import torch
from sklearn import model_selection
from toolbox_02450 import train_neural_net, draw_neural_net
from scipy import stats
import pandas as pd
from regression import *

def neural_network(X,y):
    errors_nn=np.zeros((10,2))
    y = y[:,np.newaxis]
    X = stats.zscore(X)
    
    # Parameters for neural network classifier
    hidden_units_ar = [1,2,4,6,8,10]
    n_replicates = 1        # number of networks trained in each k-fold
    max_iter = 15000
    
    # K-fold crossvalidation
    K = 3                  # only three folds to speed up this example
    CV = model_selection.KFold(K, shuffle=True)
    
    for i in range(6):
        
        mB = lambda: torch.nn.Sequential(
                            torch.nn.Linear(M, hidden_units_ar[i]), #M features to n_hidden_units
                            torch.nn.ReLU(),
                            torch.nn.Linear(hidden_units_ar[i], 1), # n_hidden_units to 1 output neuron
                            # no final tranfer function, i.e. "linear output"
                            )
        loss_fn = torch.nn.MSELoss() # notice how this is now a mean-squared-error loss
        
        print('Training model of type:\n\n{}\n'.format(str(mB())))
        errors = [] # make a list for storing generalizaition error in each loop
        for (k, (train_index, test_index)) in enumerate(CV.split(X,y)): 
            print('\nCrossvalidation fold: {0}/{1}'.format(k+1,K))    
            
            # Extract training and test set for current CV fold, convert to tensors
            X_train = torch.Tensor(X[train_index,:])
            y_train = torch.Tensor(y[train_index])
            X_test = torch.Tensor(X[test_index,:])
            y_test = torch.Tensor(y[test_index])
            
            # Train the net on training data
            net, final_loss, learning_curve = train_neural_net(mB,
                                                               loss_fn,
                                                               X=X_train,
                                                               y=y_train,
                                                               n_replicates=n_replicates,
                                                               max_iter=max_iter)
            
            print('\n\tBest loss: {}\n'.format(final_loss))
            
            # Determine estimated class labels for test set
            y_test_est = net(X_test)
            yhatB = y_test_est.detach().numpy()
            
            # Determine errors and errors
            se = (y_test_est.float()-y_test.float())**2 # squared error
            mse = (sum(se).type(torch.float)/len(y_test)).data.numpy() #mean
            errors.append(mse) # store error rate for current CV fold 
            
        # Print the average classification error rate
        print('\nEstimated generalization error, RMSE: {0}'.format(round(np.sqrt(np.mean(errors)), 4)))
        #print('\nHidden Unit: {0}'.format(h)) 
        print('\nEstimated generalization error, MSE: {0}'.format(round((np.mean(errors)), 4)))
        errors_nn[i][0]=hidden_units_ar[i]
        errors_nn[i][1]=round((np.mean(errors)), 4)
    
    min=errors_nn[0][1]
    for j in range(6):
        if min>=errors_nn[j][1]:
            min=errors_nn[j][1]
            
            
    min_errors_idx = np.where(errors_nn==min)
    h_opt = errors_nn[min_errors_idx[0][0]][0]
    mse_opt = errors_nn[min_errors_idx[0][0]][1]
    
    return mB, yhatB, y_test, mse_opt, h_opt

